/* tslint:disable no-unused-expression */
import { browser, ExpectedConditions as ec, protractor, promise } from 'protractor';
import { NavBarPage, SignInPage } from '../../page-objects/jhi-page-objects';

import { PostComponentsPage, PostDeleteDialog, PostUpdatePage } from './post.page-object';

const expect = chai.expect;

describe('Post e2e test', () => {
    let navBarPage: NavBarPage;
    let signInPage: SignInPage;
    let postUpdatePage: PostUpdatePage;
    let postComponentsPage: PostComponentsPage;
    let postDeleteDialog: PostDeleteDialog;

    before(async () => {
        await browser.get('/');
        navBarPage = new NavBarPage();
        signInPage = await navBarPage.getSignInPage();
        await signInPage.autoSignInUsing('admin', 'admin');
        await browser.wait(ec.visibilityOf(navBarPage.entityMenu), 5000);
    });

    it('should load Posts', async () => {
        await navBarPage.goToEntity('post');
        postComponentsPage = new PostComponentsPage();
        await browser.wait(ec.visibilityOf(postComponentsPage.title), 5000);
        expect(await postComponentsPage.getTitle()).to.eq('blogApp.post.home.title');
    });

    it('should load create Post page', async () => {
        await postComponentsPage.clickOnCreateButton();
        postUpdatePage = new PostUpdatePage();
        expect(await postUpdatePage.getPageTitle()).to.eq('blogApp.post.home.createOrEditLabel');
        await postUpdatePage.cancel();
    });

    it('should create and save Posts', async () => {
        const nbButtonsBeforeCreate = await postComponentsPage.countDeleteButtons();

        await postComponentsPage.clickOnCreateButton();
        await promise.all([
            postUpdatePage.setTitleInput('title'),
            postUpdatePage.setContentInput('content'),
            postUpdatePage.setDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM'),
            postUpdatePage.blogSelectLastOption()
        ]);
        expect(await postUpdatePage.getTitleInput()).to.eq('title');
        expect(await postUpdatePage.getContentInput()).to.eq('content');
        expect(await postUpdatePage.getDateInput()).to.contain('2001-01-01T02:30');
        const selectedIsPublished = postUpdatePage.getIsPublishedInput();
        if (await selectedIsPublished.isSelected()) {
            await postUpdatePage.getIsPublishedInput().click();
            expect(await postUpdatePage.getIsPublishedInput().isSelected()).to.be.false;
        } else {
            await postUpdatePage.getIsPublishedInput().click();
            expect(await postUpdatePage.getIsPublishedInput().isSelected()).to.be.true;
        }
        const selectedIsCommentAllowed = postUpdatePage.getIsCommentAllowedInput();
        if (await selectedIsCommentAllowed.isSelected()) {
            await postUpdatePage.getIsCommentAllowedInput().click();
            expect(await postUpdatePage.getIsCommentAllowedInput().isSelected()).to.be.false;
        } else {
            await postUpdatePage.getIsCommentAllowedInput().click();
            expect(await postUpdatePage.getIsCommentAllowedInput().isSelected()).to.be.true;
        }
        await postUpdatePage.save();
        expect(await postUpdatePage.getSaveButton().isPresent()).to.be.false;

        expect(await postComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeCreate + 1);
    });

    it('should delete last Post', async () => {
        const nbButtonsBeforeDelete = await postComponentsPage.countDeleteButtons();
        await postComponentsPage.clickOnLastDeleteButton();

        postDeleteDialog = new PostDeleteDialog();
        expect(await postDeleteDialog.getDialogTitle()).to.eq('blogApp.post.delete.question');
        await postDeleteDialog.clickOnConfirmButton();

        expect(await postComponentsPage.countDeleteButtons()).to.eq(nbButtonsBeforeDelete - 1);
    });

    after(async () => {
        await navBarPage.autoSignOut();
    });
});
