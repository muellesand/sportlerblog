export interface IBlog {
    id?: number;
    title?: string;
    ispublic?: boolean;
}

export class Blog implements IBlog {
    constructor(public id?: number, public title?: string, public ispublic?: boolean) {
        this.ispublic = this.ispublic || false;
    }
}
