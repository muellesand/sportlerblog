import { Moment } from 'moment';
import { IBlog } from 'app/shared/model/blog.model';

export interface IPost {
    id?: number;
    title?: string;
    content?: any;
    date?: Moment;
    isPublished?: boolean;
    isCommentAllowed?: boolean;
    blog?: IBlog;
}

export class Post implements IPost {
    constructor(
        public id?: number,
        public title?: string,
        public content?: any,
        public date?: Moment,
        public isPublished?: boolean,
        public isCommentAllowed?: boolean,
        public blog?: IBlog
    ) {
        this.isPublished = this.isPublished || false;
        this.isCommentAllowed = this.isCommentAllowed || false;
    }
}
