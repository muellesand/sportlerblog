import { Moment } from 'moment';
import { IUser } from 'app/core/user/user.model';
import { IPost } from 'app/shared/model/post.model';

export interface IComment {
    id?: number;
    title?: string;
    content?: any;
    date?: Moment;
    isApproved?: boolean;
    user?: IUser;
    post?: IPost;
}

export class Comment implements IComment {
    constructor(
        public id?: number,
        public title?: string,
        public content?: any,
        public date?: Moment,
        public isApproved?: boolean,
        public user?: IUser,
        public post?: IPost
    ) {
        this.isApproved = this.isApproved || false;
    }
}
