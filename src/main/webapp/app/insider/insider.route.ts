import { Routes } from '@angular/router';
import { InsiderComponent } from 'app/insider/insider.component';
import { UserRouteAccessService } from 'app/core';

export const insiderRoute: Routes = [
    {
        path: '',
        component: InsiderComponent,
        data: {
            pageTitle: 'insider.pageTitle',
            authorities: ['ROLE_USER']
        },
        canActivate: [UserRouteAccessService]
    }
];
