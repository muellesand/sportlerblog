import { NewsComponent } from 'app/news/news.component';
import { Routes } from '@angular/router';

export const newsRoute: Routes = [
    {
        path: '',
        component: NewsComponent,
        data: {
            authorities: ['ROLE_ANONYMOUS', 'ROLE_USER', 'ROLE_ADMIN'],
            pageTitle: 'news.pageTitle'
        }
    }
];
