import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: 'blogadmin',
                loadChildren: './blog/blog.module#BlogBlogModule'
            },
            {
                path: 'postadmin',
                loadChildren: './post/post.module#BlogPostModule'
            },
            {
                path: 'commentadmin',
                loadChildren: './comment/comment.module#BlogCommentModule'
            }

            /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
        ])
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BlogEntityModule {}
