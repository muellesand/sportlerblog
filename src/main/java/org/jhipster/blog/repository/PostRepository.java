package org.jhipster.blog.repository;

import org.jhipster.blog.domain.Post;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.data.domain.Page;

/**
 * Spring Data  repository for the Post entity.
 */

@Repository
public interface PostRepository extends JpaRepository<Post, Long>, PagingAndSortingRepository<Post, Long>{
    Page<Post> findAllByIsPublishedTrueOrderByDate(Pageable pageable);
    Page<Post> findAllByIsPublishedFalseOrderByDate(Pageable pageable);
}
