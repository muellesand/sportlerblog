package org.jhipster.blog.repository;

import org.jhipster.blog.domain.Blog;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;


/**
 * Spring Data  repository for the Blog entity.
 */

@Repository
public interface BlogRepository extends JpaRepository<Blog, Long> {
    @Query("select blog from Blog blog where blog.ispublic = true")
    List<Blog> findByBlogIsPublic();
}
